import hudson.model.*
import hudson.FilePath
import groovy.json.JsonSlurper

def getPropertiesList = { build.getEnvironment(listener).get(it).split(/(;|,|\n)/) }
def stringToBundles = { new JsonSlurper().parseText(it)['statuses'] }
def hasDifference = false

// List of environments to watch
def envUrls = getPropertiesList('ENVS_TO_SCAN')
// List of bundles to watch
def watchedBundles = getPropertiesList('BUNDLES_TO_WATCH')

envUrls.collect { new EnvEngine(it.toURL(), build) }.each {
    println '======================================================='
    println "Processing env:\t\t$it.url"

    def storedBundles = stringToBundles(it.getLocal())
    println "Recovered from storage:\t$storedBundles.size"

    String payLoad = it.getRemote()
    def diff = stringToBundles(payLoad)
            .findAll { it.state == 'Active' && watchedBundles.contains(it.name) }
            .any { bundle -> storedBundles.any {it.name == bundle.name && it.version != bundle.version} }
    hasDifference = hasDifference ?: diff
    println diff ? 'The bundle difference has been found' : 'There is no difference'

    it.put payLoad
    println '======================================================='
}

build.setResult(hasDifference ? Result.SUCCESS : Result.NOT_BUILT)

class EnvEngine {

    final URL url
    final FilePath file

    EnvEngine(URL url, def build) {
        this.url = url
        String fileName = build.workspace.toString() + url.host + '.json'
        file = build.workspace.isRemote() ?
                new FilePath(build.workspace.channel, fileName) :
                new FilePath(new File(fileName))
    }

    public put(String payLoad) {
        file.write(payLoad, null)
    };

    public String getLocal() {
        return file.exists() ? file.readToString() : '{"statuses":[]}';
    }

    public String getRemote() {
        return url.text
    }
}
